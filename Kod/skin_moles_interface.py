﻿import tkinter as tk
from tkinter import filedialog
from tkinter import *
from PIL import ImageTk, Image
import numpy
from keras.models import load_model


#model = load_model('models/')qq

labelNames = open("podaci.csv").read().strip().split("\n")[1:]

def classification(file_path):
  image = Image.open(file_path)
  image = image.resize((30, 30))
  image = numpy.expand_dims(image, axis=0)
  image = numpy.array(image)
  #pred = model.predict_classes([image])[0]
  sign = labelNames[:]#pred
  print(sign)
  result.configure(text=sign)

def show_classification_btn(file_path):
  classification_b=Button(top, text="Prepoznaj kožnu bolest", command=lambda: classification(file_path), padx=10)
  classification_b.configure(bg='yellow', fg='black', font=('arial', 10, 'bold'))
  classification_b.place(relx=0.32, rely=0.7)

def upload_image():
  try:
    file_path=filedialog.askopenfilename()
    uploaded=Image.open(file_path)
    uploaded.thumbnail(((top.winfo_width()/2.25), (top.winfo_height()/2.25)))
    im=ImageTk.PhotoImage(uploaded)
    sign_image.configure(image=im)
    sign_image.image=im
    result.configure(text='')
    show_classification_btn(file_path)
  except:
    pass

if __name__=="__main__":
  #initialise GUI
  top=tk.Tk()
  top.geometry('500x500')
  top.title('Prepoznavanje kožnih bolesti')
  top.configure(bg='#EEE8CD')
  heading = Label(top, text="Prepoznavanje kožnih bolesti (madeža)", pady=20, font=('arial', 15, 'bold'))
  heading.configure(background='#EEE8CD', fg='#030303')
  heading.pack()
  result=Label(top, font=('arial', 20, 'bold'))
  result.configure(fg='#030303', bg='#EEE8CD')
  sign_image = Label(top)
  upload=Button(top, text="Učitaj sliku", command=upload_image, padx=20, pady=10)
  upload.configure(background='yellow', fg='#030303', font=('arial', 12, 'bold'))
  upload.pack(side=BOTTOM,padx=50 ,pady=50)
  sign_image.pack(side=TOP, expand=True)
  result.pack(side=TOP, expand=True)
  top.mainloop()
