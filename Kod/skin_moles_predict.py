from fileinput import filename
import numpy as np
import matplotlib.pyplot as plt
from keras.models import load_model
import tensorflow as tf
from skin_moles_cnn_v1 import test_ds, train_ds
from skimage.transform import resize
from skimage import color
import matplotlib.image as mpimg
from skin_moles_cnn_v1 import class_names

model = load_model('models/skinmoes.h5')

image_batch, label_batch = test_ds.as_numpy_iterator().next()
predictions = model.predict_on_batch(image_batch).flatten()

predictions = tf.nn.relu(predictions)
predictions = tf.where(predictions < 0.5, 0, 1)

print('Predictions:\n', predictions.numpy())
print('Labels:\n', label_batch)

plt.figure(figsize=(10, 10))
for i in range(9):
    ax = plt.subplot(3, 3, i + 1)
    plt.imshow(image_batch[i].astype("uint8"))
    plt.title(class_names[predictions[i]] + "\n" + class_names[label_batch[i]])
    plt.axis("off")